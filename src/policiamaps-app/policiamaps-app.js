import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@johnriv/google-map/google-map.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';

import '@johnriv/google-map/google-map-directions.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-item/paper-icon-item.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/iron-icons/maps-icons.js';
import '@polymer/paper-tabs/paper-tabs.js';

/**
 * @customElement
 * @polymer
 */
class PoliciamapsApp extends PolymerElement {
  static get template() {
    return html`
  
    <style>
    paper-card {
      position: absolute;
      bottom: 25px;
      left: 25px;
      z-index: 1;
    }
      </style>
      <button on-click="obtenerOficinas" class="btn btn-success col-lg-4 col-xs-12" type="button" name="button">Obtener clientes</button>
      <google-map map="{{map}}" latitude="-12.0639213" longitude="-77.0534325"
        api-key="AIzaSyCokeRgc3_t6x0OB9_Trk7e5OGKAqyuQgw" zoom="13" disable-default-ui>

        <google-map-marker latitude="-12.0639213" longitude="-77.0534325"
        title="[[item.comisarianombre]]" draggable="true"  >
          <img src="yo1.jpg" onclick="obtenerOficinas()">
        </google-map-marker>

        <template is="dom-repeat" items="{{oficinas}}">   
        <google-map-marker latitude="[[item.comisarialat]]" longitude="[[item.comisarialong]]"
        title="[[item.comisarianombre]]" draggable="false" ></google-map-marker>
        </template>
      </google-map>

      <google-map-directions map="{{map}}"
        start-address="{{start}}"
        end-address="{{end}}" 
        travel-mode="[[travelMode]]"
        api-key="AIzaSyCokeRgc3_t6x0OB9_Trk7e5OGKAqyuQgw"></google-map-directions>


        <paper-card elevation="2"> <!-- elevation controls the amount of box shadow -->
          <paper-icon-item>
            <iron-icon icon="search" slot="item-icon"></iron-icon>
            <paper-input label="Partida" value="{{start}}"></paper-input>
          </paper-icon-item>
          <paper-icon-item>
            <iron-icon icon="search" slot="item-icon"></iron-icon>
            <paper-input label="Llegada" value="{{end}}"></paper-input>
          </paper-icon-item>

          <!-- selected="0" selects the first item in the tab list.
          Change it to another index if you want a different default. -->
          <paper-tabs selected="{{travelMode}}" attr-for-selected="label">
            <paper-tab label="DRIVING">
              <iron-icon icon="maps:directions-car"></iron-icon>
              <span>DRIVING</span>
            </paper-tab>
            <paper-tab label="WALKING">
              <iron-icon icon="maps:directions-walk"></iron-icon>
              <span>WALKING</span>
            </paper-tab>
            <paper-tab label="BICYCLING">
              <iron-icon icon="maps:directions-bike"></iron-icon>
              <span>BICYCLING</span>
            </paper-tab>
            <paper-tab label="TRANSIT">
              <iron-icon icon="maps:directions-transit"></iron-icon>
              <span>TRANSIT</span>
            </paper-tab>
          </paper-tabs>
        </paper-card>    
   




    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'policiamaps-app'
      },
      oficinas: {
        type: Array,
        notify:true
      }
    };
  }

  obtenerOficinas(){
    var request=new XMLHttpRequest();
    request.open("GET","http://localhost:3001/oficinas",false);
    request.setRequestHeader('Accept',"application/json");
    request.send();
    this.oficinas=JSON.parse(request.responseText).comisarias;
    console.log(this.oficinas);
  }
}

window.customElements.define('policiamaps-app', PoliciamapsApp);
