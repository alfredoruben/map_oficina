import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@johnriv/google-map/google-map-directions.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-item/paper-icon-item.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/iron-icons/maps-icons.js';
import '@polymer/paper-tabs/paper-tabs.js';

/**
 * @customElement
 * @polymer
 */
class CompBusqueda extends PolymerElement { 
  static get template() {
    return html`

    <google-map-directions map="{{map}}"
    start-address="{{start}}"
    end-address="{{end}}" 
    travel-mode="[[travelMode]]"
    api-key="AIzaSyCokeRgc3_t6x0OB9_Trk7e5OGKAqyuQgw"></google-map-directions>

    <paper-card elevation="2"> <!-- elevation controls the amount of box shadow -->
    <paper-icon-item>
      <iron-icon icon="search" slot="item-icon"></iron-icon>
      <paper-input label="Start address" value="{{start}}"></paper-input>
    </paper-icon-item>
    <paper-icon-item>
      <iron-icon icon="search" slot="item-icon"></iron-icon>
      <paper-input label="End address" value="{{end}}"></paper-input>
    </paper-icon-item>

    <!-- selected="0" selects the first item in the tab list.
    Change it to another index if you want a different default. -->
    <paper-tabs selected="{{travelMode}}" attr-for-selected="label">
      <paper-tab label="DRIVING">
        <iron-icon icon="maps:directions-car"></iron-icon>
        <span>DRIVING</span>
      </paper-tab>
      <paper-tab label="WALKING">
        <iron-icon icon="maps:directions-walk"></iron-icon>
        <span>WALKING</span>
      </paper-tab>
      <paper-tab label="BICYCLING">
        <iron-icon icon="maps:directions-bike"></iron-icon>
        <span>BICYCLING</span>
      </paper-tab>
      <paper-tab label="TRANSIT">
        <iron-icon icon="maps:directions-transit"></iron-icon>
        <span>TRANSIT</span>
      </paper-tab>
    </paper-tabs>
  </paper-card> 

    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'comp-busqueda'
      },
      oficinas: {
        type: Array,
        notify:true
      }
    };
  }


}

window.customElements.define('comp-busqueda', CompBusqueda);
